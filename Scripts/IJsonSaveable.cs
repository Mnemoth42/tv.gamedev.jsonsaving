using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace GameDevTV.Saving
{
    public interface IJsonSaveable
    {
        /// <summary>
        /// Override to return a JToken representing the state of the IJsonSaveable
        /// </summary>
        /// <returns>A JToken</returns>
        JToken CaptureAsJToken();
        /// <summary>
        /// Restore the state of the component using the information in JToken.
        /// </summary>
        /// <param name="state">A JToken object representing the state of the module</param>
        void RestoreFromJToken(JToken state);
    }

    public static class JsonStatics
    {
        /// <summary>
        /// Extension method to convert a Vector3 value into a JToken.
        /// <example>
        /// <code>
        /// Vector3 vector = new Vector3(10,20,10);
        /// JToken token = vector.ToToken();
        /// </code>
        /// </example> 
        /// </summary>
        /// <param name="vector">this Vector3 to encode</param>
        /// <returns>A JToken</returns>
        public static JToken ToToken(this Vector3 vector)
        {
            JObject state = new JObject();
            IDictionary<string, JToken> stateDict = state;
            stateDict["x"] = vector.x;
            stateDict["y"] = vector.y;
            stateDict["z"] = vector.z;
            return state;
        }

        /// <summary>
        /// Converts a JToken value into a Vector3.  Will return a zero vector
        /// if the JToken is not a tokenized Vector3 returned by a vector3.ToToken()
        /// <example>
        /// (where token is a JToken)
        /// <code>
        /// Vector3 vector = token.ToVector3();
        /// </code></example>
        /// </summary>
        /// <param name="state">A JToken representing the vector</param>
        /// <returns>A Vector3</returns>
        public static Vector3 ToVector3(this JToken state)
        {
            Vector3 vector = new Vector3();
            if (state is JObject jObject)
            {
                IDictionary<string, JToken> stateDict = jObject;

                if (stateDict.TryGetValue("x", out JToken x))
                {
                    vector.x = x.ToObject<float>();
                }

                if (stateDict.TryGetValue("y", out JToken y))
                {
                    vector.y = y.ToObject<float>();
                }

                if (stateDict.TryGetValue("z", out JToken z))
                {
                    vector.z = z.ToObject<float>();
                }
            }

            return vector;
        }
    }

}